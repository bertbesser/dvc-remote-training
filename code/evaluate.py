import json
import onnxruntime as rt
import numpy as np

from pathlib import Path
from sklearn.metrics import accuracy_score, precision_score, recall_score, f1_score

from preprocessing import get_test_data_generator, get_img_array_from_path
from postprocessing import get_class_prediction

DIR_PATH = Path().absolute()
PATH_TO_TEST = Path.joinpath(DIR_PATH, 'data/fruits/test')
PATH_TO_MODELS = Path.joinpath(DIR_PATH, 'models')

with open(Path.joinpath(PATH_TO_MODELS, 'model_config.json')) as json_file:
    model_config = json.load(json_file)

normalize_pixel_values = model_config['normalize_pixel_values']
target_size = model_config['target_size']

test_datagen = get_test_data_generator(False)
test_generator = test_datagen.flow_from_directory(
        PATH_TO_TEST,  
        target_size=target_size,  
        batch_size=1,
        class_mode=None,
        shuffle=False)

sess = rt.InferenceSession(str(Path.joinpath(PATH_TO_MODELS, 'model.onnx')))

y_pred = []
for filepath in test_generator.filepaths:
    img_array = get_img_array_from_path(
        filepath, 
        normalize_pixel_values= normalize_pixel_values, 
        target_size = target_size)

    pred_probas = sess.run(
        None, 
        {sess.get_inputs()[0].name: img_array}
        )[0]
    
    y_pred.append(np.argmax(pred_probas, axis=1)) 

y_true = test_generator.classes

metrics = {
    "accuracy_score": accuracy_score(y_true, y_pred),
    "precision_score": precision_score(y_true, y_pred, average='micro'),
    "recall_score": recall_score(y_true, y_pred, average='micro'),   
    "f1_score": f1_score(y_true, y_pred, average='micro')   
}

with open(Path.joinpath(PATH_TO_MODELS, 'model_metrics.json'), 'w') as outfile:
    json.dump(metrics, outfile)