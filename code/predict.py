import json
import onnxruntime as rt

from pathlib import Path
from preprocessing import get_img_array_from_path
from postprocessing import get_class_prediction

DIR_PATH = Path().absolute()
PATH_TO_IMG = Path.joinpath(DIR_PATH, 'data/fruits/training/Kiwi/275_100.jpg')
PATH_TO_MODEL = Path.joinpath(DIR_PATH, 'models/model.onnx')
PATH_TO_CONFIG = Path.joinpath(DIR_PATH, 'models/model_config.json')

with open(PATH_TO_CONFIG) as json_file:
    model_config = json.load(json_file)

labels = model_config['labels']
normalize_pixel_values = model_config['normalize_pixel_values']
target_size = model_config['target_size']

sess = rt.InferenceSession(str(PATH_TO_MODEL))

img_array = get_img_array_from_path(
    PATH_TO_IMG, 
    normalize_pixel_values= normalize_pixel_values, 
    target_size = target_size)

pred_probas = sess.run(
    None, 
    {sess.get_inputs()[0].name: img_array}
    )[0]

print(get_class_prediction(labels=labels, pred_probas=pred_probas))